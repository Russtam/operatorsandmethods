﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/*----------------------------------------------------
 * Данный класс позволяет производить операции со временем
 * В классе реализованы операции сложения, разности и сравнения.
 * Реализован класс Times, который позволяет создавать массив с объектов Time
 * Реализован индексатор, который позволяет манипулировать с объектами массива
 * Реализован метод расширения для класса string 
 * Данный класс реализован в качестве решения домашнего задания, с 
 * целью показать освоение прошедшей темы.
 */

namespace Lesson2
{
    /// <summary>
    /// Класс время
    /// </summary>
    public class Time
    {
        private int hour, minute, second;

        /// <summary>
        /// Конструктор времени
        /// </summary>
        /// <param name="hours">часы</param>
        /// <param name="minutes">минуты</param>
        /// <param name="seconds">секунды</param>
        public Time(int hours, int minutes, int seconds)
        {
            Hours = hours;
            Minutes = minutes;
            Seconds = seconds;
        }
        /// <summary>
        /// Часы
        /// </summary>
        public int Hours
        {
            get => hour;
            set => hour = value;
        }
        /// <summary>
        /// Минуты
        /// </summary>
        public int Minutes
        {
            get => minute;
            set => minute = value;
        }
        /// <summary>
        /// Секунды
        /// </summary>
        public int Seconds
        {
            get => second;
            set => second = value;
        }

        /// <summary>
        ///  оператор сложения времени
        /// </summary>
        /// <param name="firstTime">первый показатель</param>
        /// <param name="secondTime">второй показатель</param>
        /// <returns></returns>
        public static TimeSpan operator + (Time firstTime, Time secondTime)
        {
            var firstToSeconds = ConvertToSeconds(firstTime);
            var secondToSeconds = ConvertToSeconds(secondTime);

            //return firstToSeconds + secondToSeconds;  наверно так не стоит делать, чтоб не запутаться
            //Функция конвертирования времени в секунды.Создана локально только чтоб проверить работу C# 8
            int ConvertToSeconds(Time time)
            {
                var hourToSeconds = time.hour * 60 * 60;
                var minutesToSeconds = time.Minutes * 60;
                return hourToSeconds + minutesToSeconds + time.Seconds;
            }
            return TimeSpan.FromSeconds(firstToSeconds + secondToSeconds);
        }
        /// <summary>
        /// оператор нахождения разности времени
        /// </summary>
        /// <param name="firstTime"></param>
        /// <param name="secondTime"></param>
        /// <returns></returns>
        public static int operator - (Time firstTime, Time secondTime)
        {
            var firstToSeconds = ConvertToSeconds(firstTime);
            var secondToSeconds = ConvertToSeconds(firstTime);
            if (firstToSeconds < secondToSeconds)
                throw new Exception("Конечное время не может быть меньше, чем начальное");
            //Функция конвертирования времени в секунды

            return secondToSeconds - firstToSeconds;
        }
        /// <summary>
        /// сравнение показателей времени на равенство
        /// </summary>
        /// <param name="time1"></param>
        /// <param name="time2"></param>
        /// <returns>являются ли два показателя одинаковыми</returns>
        public static bool operator ==(Time time1, Time time2)
        {
            var firstToSeconds = ConvertToSeconds(time1);
            var secondToSeconds = ConvertToSeconds(time2);
            if (firstToSeconds == secondToSeconds)
                return true;
            return false;
        }

        public static bool operator !=(Time time1, Time time2)
        {
            var firstToSeconds = ConvertToSeconds(time1);
            var secondToSeconds = ConvertToSeconds(time2);
            if (firstToSeconds == secondToSeconds)
                return false;
            return true;
        }

        /// <summary>
        /// Вывод времени в формате 00:00:00
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"{Hours}:{Minutes}:{Seconds}";
        /// <summary>
        /// конвертирование времени в сеекунды
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        static int ConvertToSeconds(Time time)
        {
            var hourToSeconds = time.hour * 60 * 60;
            var minutesToSeconds = time.Minutes * 60;
            return hourToSeconds + minutesToSeconds + time.Seconds;
        }
    }
    /// <summary>
    /// Массив показателей времени
    /// </summary>
    public class Times
    {
        Time[] times;
        public int Length { get; set; }

        public Times(Time[] times)
        {
            this.times = times;
            Length = times.Length;
        }
        /// <summary>
        /// индексатор массива показателей времени
        /// </summary>
        /// <param name="index">индекс в массиве</param>
        /// <returns></returns>
        public Time this[int index]
        {
            get
            {
                return times[index];
            }
            set
            {
                times[index] = value;
            }
        }
    }
    /// <summary>
    /// Расширяющий метод для класса string
    /// </summary>
    public static class TimeExtension
    {
        /// <summary>
        /// Метод возвращает время если строка была передана в формате 00-00-00, 00.00.00, 00:00:00
        /// </summary>
        /// <param name="time">Время</param>
        /// <returns></returns>
        public static Time GetTime(this string time)
        {
            if (time.Contains('.'))
            {
                var timeMass = time.Split('.').Select(x=>Convert.ToInt32(x)).ToArray();
                if (timeMass.Count() > 3)
                    throw new Exception("Строка не соответствует формату времени 00.00.00");
                return new Time(timeMass[0], timeMass[1], timeMass[2]);
            }
            else if (time.Contains('-'))
            {
                var timeMass = time.Split('-').Select(x => Convert.ToInt32(x)).ToArray();
                if (timeMass.Count() > 3)
                    throw new Exception("Строка не соответствует формату времени 00-00-00");
                return new Time(timeMass[0], timeMass[1], timeMass[2]);
            }
            else if (time.Contains(':'))
            {
                var timeMass = time.Split(':').Select(x => Convert.ToInt32(x)).ToArray();
                if (timeMass.Count() > 3)
                    throw new Exception("Строка не соответствует формату времени 00-00-00");
                return new Time(timeMass[0], timeMass[1], timeMass[2]);
            }
            return default;
        }
    }

}
