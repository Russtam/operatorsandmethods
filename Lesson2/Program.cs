﻿using System;

namespace Lesson2
{
    class Program
    {
        static void Main(string[] args)
        {
            var time1 = "10.00.21".GetTime();
            var time2 = "10-12-23";
            var time3 = "12:34:12";
            Time time = time3.GetTime();
            Time time4 = new Time(13, 15, 46);
            var concatTime = time + time1;
            Console.WriteLine(concatTime.ToString());
            var timeMass = new Times(new[] { time1, time2.GetTime(), time, time4 });
            Console.WriteLine(time.ToString());

            Console.WriteLine("Отображение через цикл");
            for(int i=0;i<timeMass.Length; i++)
                Console.WriteLine(timeMass[i].ToString());
            Console.WriteLine(time1 != time);
            Console.WriteLine(time1 == time);
            Console.ReadKey();
        }
    }

}
